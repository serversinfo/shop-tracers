#pragma semicolon 1
#include <sourcemod>
#include <sdktools>
#include <shop>


#pragma newdecls required

#define LIFE	0.2
#define WIDTH	2.0

Handle g_hKv;

 //g_bEnabled[MAXPLAYERS+1],
	bool g_bHide;
	bool g_bPrecached;

int g_iSprites[10];
int g_iSprite[MAXPLAYERS+1];
int g_iColor[MAXPLAYERS+1][4];

public Plugin myinfo =
{
	name = "[Shop] Color Tracers",
	author = "FrozDark & R1KO (HLModders LLC)",
	version = "2.0.3",
	url  = "www.hlmod.ru"
};

public void OnPluginStart()
{
	HookEvent("bullet_impact", Event_BulletImpact);
	HookEvent("player_hurt", Event_PlayerHurt);

	AutoExecConfig(true, "shop_color_tracers", "shop");

	if (Shop_IsStarted()) Shop_Started();
	PrecacheModels();
}

public void OnPluginEnd()
{
	Shop_UnregisterMe();
}

public void OnMapStart()
{
	if (g_hKv != INVALID_HANDLE) CloseHandle(g_hKv);

	char buffer[PLATFORM_MAX_PATH];

	g_hKv = CreateKeyValues("Tracers");

	Shop_GetCfgFile(buffer, sizeof(buffer), "trasers.txt");

	if (!FileToKeyValues(g_hKv, buffer)) SetFailState("Couldn't parse file %s", buffer);
	g_bPrecached = false;
	PrecacheModels();
	//KvGetString(g_hKv, "material", buffer, sizeof(buffer), "materials/sprites/laserbeam.vmt");
	//g_iSprites[0] = PrecacheModel(buffer);
}

public int Shop_Started()
{
	if (g_hKv == INVALID_HANDLE) OnMapStart();
	
	
	
	KvRewind(g_hKv);
	char sName[64], sDescription[64];
	g_bHide = view_as<bool>(KvGetNum(g_hKv, "hide_opposite_team"));
	KvGetString(g_hKv, "name", sName, sizeof(sName), "Color Tracers");
	KvGetString(g_hKv, "description", sDescription, sizeof(sDescription));

	CategoryId category_id = Shop_RegisterCategory("color_tracers", sName, sDescription);

	KvRewind(g_hKv);

	if (KvGotoFirstSubKey(g_hKv)) {
		do {
			if (KvGetSectionName(g_hKv, sName, sizeof(sName)) && !StrEqual(sName, "materials") && Shop_StartItem(category_id, sName)) {
				KvGetString(g_hKv, "name", sDescription, sizeof(sDescription), sName);
				Shop_SetInfo(sDescription, "", KvGetNum(g_hKv, "price", 1000), KvGetNum(g_hKv, "sellprice", -1), Item_Togglable, KvGetNum(g_hKv, "duration", 604800));
				Shop_SetCustomInfo("level", KvGetNum(g_hKv, "level", 0));
				Shop_SetCallbacks(_, OnTracersUsed);
				Shop_EndItem();
			}
		} while (KvGotoNextKey(g_hKv));
	}
	
	KvRewind(g_hKv);
}

void PrecacheModels() {
	if (g_bPrecached)
		return;
	KvRewind(g_hKv);
	char si[3], sName[64], sSprite[128];
	if (KvGotoFirstSubKey(g_hKv)) {
		do {
			if (KvGetSectionName(g_hKv, sName, sizeof(sName)) && StrEqual(sName, "materials")) {
				for(int i=0; i<10;i++) {
					IntToString(i, si, sizeof(si));
					KvGetString(g_hKv, si, sSprite, sizeof(sSprite), "none");
					char sNone[] = "none";
					if (StrEqual(sSprite, sNone))
						break;
					g_iSprites[i] = PrecacheModel(sSprite);
				}
			}
		} while (KvGotoNextKey(g_hKv));
		g_bPrecached = true;
	}
}

public void OnClientPostAdminCheck(int iClient) {
	g_iSprite[iClient] = 0;
}

public ShopAction OnTracersUsed(int iClient, CategoryId category_id, const char[] category, ItemId item_id, const char[] item, bool isOn, bool elapsed)
{
	if (isOn || elapsed) {
		g_iSprite[iClient] = 0;
		return Shop_UseOff;
	}

	Shop_ToggleClientCategoryOff(iClient, category_id);
	KvRewind(g_hKv);
	if(KvJumpToKey(g_hKv, item, false)) {
		KvGetColor(g_hKv, "color", g_iColor[iClient][0], g_iColor[iClient][1], g_iColor[iClient][2], g_iColor[iClient][3]);
		char sI[3];
		int i;
		if(KvGetString(g_hKv, "model", sI, sizeof(sI))) {
			i = StringToInt(sI);
			g_iSprite[iClient] = g_iSprites[i];
		} else g_iSprite[iClient] = g_iSprites[0];
		
		return Shop_UseOn;
	}
	KvRewind(g_hKv);
	
	return Shop_Raw;
}

public Action Event_PlayerHurt(Event hEvent, const char[] weaponName, bool dontBroadcast)
{
	int iClient = GetClientOfUserId(GetEventInt(hEvent, "userid"));
	if (iClient && g_iSprite[iClient]) {
		int iDamage = GetEventInt(hEvent, "dmg_health");
		SetEventInt(hEvent, "dmg_health", RoundToNearest(iDamage*1.2));
		return Plugin_Continue;
	}
	return Plugin_Continue;
}

public void Event_BulletImpact(Handle hEvent, const char[] weaponName, bool dontBroadcast)
{
	int iClient = GetClientOfUserId(GetEventInt(hEvent, "userid"));

 	if (iClient && g_iSprite[iClient]) {
		float bulletOrigin[3], newBulletOrigin[3];
		int clients[MAXPLAYERS+1];
		int i, totalClients;
		GetClientEyePosition(iClient, bulletOrigin);

		float bulletDestination[3];
		bulletDestination[0] = GetEventFloat(hEvent, "x");
		bulletDestination[1] = GetEventFloat(hEvent, "y");
		bulletDestination[2] = GetEventFloat(hEvent, "z");

		float distance = GetVectorDistance( bulletOrigin, bulletDestination );

		float percentage = 0.4 / ( distance / 100 );
 
		newBulletOrigin[0] = bulletOrigin[0] + ((bulletDestination[0] - bulletOrigin[0]) * percentage);
		newBulletOrigin[1] = bulletOrigin[1] + ((bulletDestination[1] - bulletOrigin[1]) * percentage) - 0.08;
		newBulletOrigin[2] = bulletOrigin[2] + ((bulletDestination[2] - bulletOrigin[2]) * percentage);

		TE_SetupBeamPoints(newBulletOrigin, bulletDestination, g_iSprite[iClient], 0, 0, 0, LIFE, WIDTH, WIDTH, 1, 0.0, g_iColor[iClient], 0);

		i = 1;
		totalClients = 0;
		
		if(g_bHide) {
			int iTeam = GetClientTeam(iClient);
			while(i <= MaxClients) {
				if(IsClientInGame(i) && IsFakeClient(i) == false && GetClientTeam(i) == iTeam)
					clients[totalClients++] = i;
				++i;
			}
		}
		else while(i <= MaxClients) { 
			if(IsClientInGame(i) && IsFakeClient(i) == false)
				clients[totalClients++] = i;
			++i;
		}

		TE_Send(clients, totalClients);
	}
}